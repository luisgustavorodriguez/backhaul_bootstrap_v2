#Backhaul Bootstrap

## Resumen
Este repositorio contiene los scripts y archivos de configuracion para el despliegue de CeraOptimizer Backhaul en diferentes dominios
Cada directorio, a excepcion de los submodulos __deploy__ y __backup_scripts__, representa un dominio y contiene una configuracion particular.


## Despliegue de la aplicacion
1. Clonar este repositorio utilizando credenciales validas de Bitbucket:
   
        $ git clone https://<user>@bitbucket.org/lcduncteam/backhaul_bootstrap_v2.git

2. Ejecutar el script que se encuentra en el directorio recientemente clonado añadiendo como argumento el nombre de la instancia (carpeta). El mismo se encarga de:


  * Descargar las imagenes docker / cargarlas desde archivos.
  * Detener el sistema, si es que estuviera corriendo.
  * Desplegar el sistema.
  * Crear el directorio donde se almacenaran los archivos de backup.
  * Proponer agregar las tareas de creacion de backups al programador de tareas. ___(Solo debe hacerse al desplegar el sistema por primera vez.)___
  
        $ cd backhaul_bootstrap_v2/deploy
        $ ./deploy.sh <instancia>    

    Por ej: si estuvieramos desplegando en el servidor de testing:  
  
        $ ./deploy.sh testing

    Si estuvieramos desplegando de manera local (127.0.0.1):
  
        $ ./deploy.sh default

## Descripcion del funcionamiento
El repositorio cuenta con 2 submodulos comunes a todos los ___bootstrap__:

DEPLOY:
 * Archivos .yml (con variables parametrizadas) los cuales hacen referencia a los servicios utilizados por la app.
 * Script de despliegue standar (deploy.sh)
 * Variables de entorno standar (common.env)

BACKUP_SCRIPTS
* Scripts de creacion de backup del volumen _media_ y las bases de datos _postgres_ y _mongo_.
* Script de restauración standar de base de datos _postgres_ o _mongo_.


### Archivos _.env_
Definen las variables de entorno que se utilizarán (mayormente) en los archivos .yml  
Al ejecutarse el script de despliegue los archivos con variables de entorno se cargarán en el siguiente orden:
1. _deploy/common.env_ (variables comunes a todos los sistemas)
2. _default/common.env_ (configuracion estandar a backhaul)
3. _instancia/prod.env_ (configuracion especifica para la instancia seleccionada)

### Archivos _stack_specs.sh_
Definen que imagenes docker descargar y cuales archivos .yml utilizar en el stack.  
Inicialmente se intentara buscar el archivo dentro de la carpeta de la instancia seleccionada para desplegar. En caso de no encontrarlo, se utilizara uno por defecto ubicado en la carpeta __/default__.

## Añadir nuevo dominio de despliegue
En caso que se desee añadir un nuevo dominio a donde desplegar se deberán seguir los siguientes pasos:
1. Crear una carpeta con el nombre del dominio
2. Dentro de la carpeta recientemente creada crear un archivo _prod.env_ con la configuracion especifica (host, cantidad de replicas, puertos, etc). Tenes en cuenta que variables con el mismo nombre pisaran los valores de las mismas en _deploy/common.env_ y _default/common.env_
3. En caso de no querer desplegar el stack completo (con todas las imagenes) crear un archivo _stack_specs.sh_. Se recomienda copiar el que se encuentra en _default/stack_specs.sh_ y editarlo.

## Despliegue en sistemas sin conexion a Internet (brasil_tim)
En el caso particular que el servidor en donde se desee desplegar no posea conexion a Internet no se podran descargar las imagenes desde hub.docker.  
La solución: guardar las imagenes docker (descargadas/generadas localmente) en archivos y luego copiar esos archivos al servidor en cuestion.

Para ello existe el script _save_images.sh_. El mismo guardara en .tar.gzip las imagenes necesarias para el despliegue. Tener en cuenta que estas imagenes deben existir en la PC donde se ejecutara el script.

Si tomamos como ejemplo _brasil_tim_, el script _stack_specs.sh_ no descarga las imagenes desde la web, sino que las carga desde archivos.