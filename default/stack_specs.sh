#!/bin/bash

# DEFINE WHICH IMAGES INCLUDE IN THE STACK
# TO ADD/REMOVE IMAGES (i.e. Kibana) EDIT "docker pull" and COMPOSE_FILES lines

echo ""; read -e -p "Pull images from Docker hub? (y/n): " -i "y" option_pull
if [ 'y' == "$option_pull" ]; then
  echo "Pulling Docker images..."
  echo "-----------"
  docker pull ${IMAGE}
  docker pull unclcd/backhaul_data_service:latest
  docker pull unclcd/backhaul_collector_worker:latest
  docker pull unclcd/backhaul_cron:latest
  docker pull unclcd/cap_solver_service_lite:latest

  docker pull ankane/pghero
  docker pull unclcd/nginx:latest
  docker pull unclcd/postgres:10.5-alpine
  docker pull mongo:4.2
  docker pull unclcd/monitor_service:latest
  docker pull docker.elastic.co/kibana/kibana:7.3.2
  docker pull docker.elastic.co/elasticsearch/elasticsearch:7.3.2
  docker pull unclcd/logstash:latest
  docker pull unclcd/filebeat:latest
  docker pull unclcd/elk_config:latest
fi

COMPOSE_FILES='-c deploy/db.yml -c deploy/web.yml -c deploy/mongodb.yml -c deploy/data_service.yml -c deploy/monitor_service.yml -c deploy/collector.yml -c deploy/kibana.yml -c deploy/db_stats.yml -c deploy/cap_solver.yml -c deploy/cron.yml -c default/backhaul.yml'
