#!/bin/bash

# This script saves essential backhaul docker images to file

echo "Saving backhaul..."
docker save unclcd/backhaul:latest | gzip > backhaul:latest.tar.gzip

echo "Saving nginx..."
docker save unclcd/nginx:latest | gzip > nginx:latest.tar.gzip

echo "Saving postgres..."
docker save unclcd/postgres:10.5-alpine | gzip > postgres:10.5-alpine.tar.gzip

echo "Saving mongo..."
docker save mongo:4.2 | gzip > mongo:4.2.tar.gzip

#echo "Saving monitor_service..."
#docker save unclcd/monitor_service:latest | gzip > monitor_service:latest.tar.gzip

echo "Saving backhaul_data_service..."
docker save unclcd/backhaul_data_service:latest | gzip > backhaul_data_service:latest.tar.gzip

#echo "Saving backhaul_collector_worker..."
#docker save unclcd/backhaul_collector_worker:latest | gzip > backhaul_collector_worker:latest.tar.gzip