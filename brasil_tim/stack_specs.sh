#!/bin/bash

# DEFINE WHICH IMAGES INCLUDE IN THE STACK
# TO ADD/REMOVE IMAGES (i.e. Kibana) EDIT "docker pull" and COMPOSE_FILES lines

echo ""; read -e -p "Load images from files? (y/n): " -i "y" option_pull
if [ 'y' == "$option_pull" ]; then
  read -e -p "Enter images location path: " option_path
  echo "Loading docker images..."
  echo "-----------"
  docker load < "${option_path}/backhaul:latest.tar.gzip"
  docker load < "${option_path}/nginx:latest.tar.gzip"
  docker load < "${option_path}/postgres:10.5-alpine.tar.gzip"
  docker load < "${option_path}/mongo:4.2.tar.gzip"
  #docker load < "${option_path}/monitor_service:latest.tar.gzip"
  docker load < "${option_path}/backhaul_data_service:latest.tar.gzip"
  #docker load < "${option_path}/backhaul_collector_worker:latest.tar.gzip"
fi

COMPOSE_FILES='-c deploy/db.yml -c deploy/web.yml -c deploy/mongodb.yml -c deploy/data_service.yml -c deploy/monitor_service.yml -c deploy/collector.yml -c default/backhaul.yml'

